# Настройка браузера Google Ghrome на OS Linux

Поверхностное описание про эксплойты и с чем их едят, вы найдёте на сайте [Kaspersky](https://www.kaspersky.ru/blog/exploits-problem-explanation/8459/). А вот как бороться с НЕКОТОРЫМИ уязвимостями будет описано тут: Данная инструкция предназначена не для пользователей Windows, к сожалению вам помочь нечем, покупайте антивирус!В данной стать я опишу как усилит защиту пользователям Linux, и поборемся с НЕКОТОРЫМИ уязвимостями!  Значит у вас ОС Linux, в качестве основного браузера вы используете Google Ghrome.

# Для начала настроим ОС, для этого выполним несколько действий.

* Редактируем конфигурационный файл fstab

```
vi /etc/fstab

#Дописываем:
#tmpfs /tmp tmpfs acl,noexec,rw,nosuid,nodev,relatime 0 0
#У меня по проще:
tmpfs /tmp tmpfs  nosuid,nodev,relatime  0 0
```

Важными параметрами здесь является acl и noexec. Второй параметр не позволит запускать программы и скрипты из директории /tmp.То что мы я монтирую tmp в  tmpfs мне так удобно но это не обязательно.С остальными параметрами знакомьтесь через google.Примечание: Некоторые дистрибутивы Linux по умолчанию создают отдельную точку монтирования для директории tmp, и тогда вам просто нужно дописать некоторые параметры.Тат как я монтирую свой tmp в оперативную память, то мне нужно избавится от артефактов и перезагрузить компьютер, можно конечно перемонтировать директорию, но я же чайник!

```
rm -rf /tmp/*
reboot -f 
```

# После загрузки компьютера приступаем к настройке Google Ghrome!

Первым делом посещаем сайт по этим ссылкам:

* * Настройка chromium policies на Linux
* * Policy List кстати, если хорошо поищите найдете на русском языке, на этом же сайте.

На каких браузерах работает chromium policies, на всех chromi-подобных, посему перечисляю: Сhromium и его старший брат Google Ghrome и их пересобранные дети - всех и не помню, Vivaldi и даже хваленный Yandex browser (но к сожалению путь до директории не подскажу), я не пробовал только на нынешней Opere (не нравится она мне).

Т.к. у меня Google Ghrome, опишу настройки для данного браузера!

```
mkdir -p /etc/opt/chrome/policies/{managed,recommended,theme}
```
Директория recommended нам точно не нужна, а вот в директории theme я создаю свои темы, но об этом не в данной статье.Создаем свой Policy List:

```
vi /etc/opt/chrome/policies/managed/crome_my.json 
chmod ugo+rwx -R /etc/opt/chrome
```
Примечание: с правами можете пошаманить, я заморачиваться не стал, хотя от офисных амёб лучше защитится!
Особо вдаваться в подробности не буду, прокомментирую только один пункт, с остальными разбирайтесь сами. Сам файл в формате json, но иногда поддерживает комментарии, но лучше их там не делать, также не оставляйте пустые строки и обратите внимание на последние две строки. Так как файл у меня универсальный для разных версий  Google Ghrome, то вы найдете устаревшие опции, но это не страшно, если опция не работает на данной версии Google Ghrome она пропускается. Хух, еле сказал!
Параметр который я опишу это DiskCacheDir, это то параметр из-за которого пришлось писать данную статью.

```
"DiskCacheDir":"/tmp/chrome_cache/${machine_name}/${user_name}",
```
Как вы уже поняли мы говорим Google Ghrome хранить кэш в директории /tmp,но так как наш компьютер для корпоративного сектора, то у нас может быть несколько пользователей, поэтому отделяем их друг от друга переменной user_name (эти переменные в формате браузера, так что не путайте).
Примечание: Как приятный бонус, в данном конфигурационном файле отключается автоматический запуск микрофона и веб камеры, так же некоторые пользователи хороших видеокарт страдают от ряби при просмотре и перелистывании, - отключаем 3D и акселерацию, уверяю на производительности не отразится, только если в лучшую сторону.  Так же отключается работа браузера как службы, это настройка злоумышленникам предоставляет удаленный рабочий стол, правда на Windows, а также может крутить какой нибудь скрипт в фоне. И вообще опция требуется только если у вас какой то сервис использует API, но не как не для пользователей рабочего стола, на скорость запуска браузера не влияет! Кстати, некоторые параметры и конфигурации ниже влияют на скорость работы вечно тупящего Google Ghrome, - лично у меня не тупит!

```json
cat /etc/opt/chrome/policies/managed/crome_my.json   
{ 
  "DeveloperToolsDisabled":true, 
  "DefaultSearchProviderEnabled":true, 
  "DefaultSearchProviderName":["Google"], 
  "ShowHomeButton":false, 
  "DisablePrintPreview":true, 
  "PrintingEnabled":true, 
  "DisableScreenshots":true, 
  "BackgroundModeEnabled":false, 
  "ClearSiteDataOnExit":true, 
  "HardwareAccelerationModeEnabled":false, 
  "Disable3DAPIs":true, 
  "EnableMediaRouter":false,
  "DisabledPlugins":["Java", "Shockwave Flash", "Chrome PDF Viewer", "Chromoting Viewer"], 
  "HideWebStoreIcon":true, 
  "HideWebStorePromo":true, 
  "ImportAutofillFormData":false, 
  "ImportBookmarks":false, 
  "ImportHistory":false, 
  "ImportHomepage":false, 
  "InstantEnabled":false, 
  "MetricsReportingEnabled":false, 
  "QuicAllowed":false, 
  "SearchSuggestEnabled":false, 
  "PasswordManagerEnabled":true, 
  "PasswordManagerAllowShowPasswords":false,
  "RemoteAccessHostFirewallTraversal":false, 
  "RemoteAccessHostDomain":["domain.local"],
  "RemoteAccessHostClientDomain":["domain.local"],
  "DefaultCookiesSetting":4, 
  "DefaultGeolocationSetting":2, 
  "DefaultMediaStreamSetting":2,
  "DefaultWebBluetoothGuardSetting":2, 
  "SupervisedUserCreationEnabled":false,
  "AllowDinosaurEasterEgg":true, 
  "BrowserAddPersonEnabled":true,   
  "BrowserGuestModeEnabled":false,  
  "BuiltInDnsClientEnabled":false, 
  "DnsPrefetchingEnabled":true, 
  "CloudPrintProxyEnabled":false, 
  "CloudPrintSubmitEnabled":false, 
  "DefaultBrowserSettingEnabled":false, 
  "DisablePluginFinder":true, 
  "ForceEphemeralProfiles":false, 
  "FullscreenAllowed":false, 
  "EnableMediaRouter":false, 
  "NativeMessagingUserLevelHosts":false,
  "AllowCrossOriginAuthPrompt":false,
  "AutofillCreditCardEnabled":false, 
  "BrowserNetworkTimeQueriesEnabled":false,
  "ComponentUpdatesEnabled":false,   
  "ForceYouTubeRestrict":0, 
  "PrintPreviewUseSystemDefaultPrinter":false,      
  "RunAllFlashInAllowMode":false,   
  "SafeBrowsingExtendedReportingOptInAllowed":false,   
  "VideoCaptureAllowed":true, 
  "VideoCaptureAllowedUrls":["https://my.local.local/"],  
  "BlockThirdPartyCookies":true, 
  "DownloadRestrictions":0, 
  "ForceGoogleSafeSearch":false, 
  "SitePerProcess":true, 
  "AdsSettingForIntrusiveAdsSites":2,   
  "AbusiveExperienceInterventionEnforce":true,  
  "SafeBrowsingEnabled":true, 
  "TaskManagerEndProcessEnabled":true,  
  "DiskCacheDir":"/tmp/chrome_cache/${machine_name}/${user_name}", 
  "DownloadDirectory":"/home/${user_name}/HOME_DIR/Downloads/" 
}
```

И последнее что мы должны сделать, это закрыть браузер если он у вас сейчас запущен, и убедится что не работает в фоне, в общем убейте процесс.  

```
pkill -9 chrome
```

Далее удаляем две директории из профиля пользователя.

```
# Директория с кэшем
rm -rf /home/$USER/.cache/google-chrome
# Директория с настройками браузера.
# Примечания: Будут удалены все настройки и сохранённые пароли. 
rm -rf  /home/$USER/.config/google-chrome
```

Если не удалить директории, то половина правил не применяется.
Далее делаем запуск с очисткой кеша:
Редактируем скрипт запуска, можно сделать обвязочку, но я заморачиваться не стал. 

```
vim /usr/bin/google-chrome

#В самом начале дописываем

#!/bin/bash
#
rm -rf /home/$USER/.cache/google-chrome
rm -rf /tmp/chrome_cache/linux-hdsv/$USER

# если у вас хомяк монтируется, то нужно позаботится о проверки путей, иначе скриптик вам чтонибудь сломает!
# Короче вот так:
# Теперь при каждом запуске или перезапуске кеш будет вычещен.
# Полезно когда браузер подвисает от количества открытых вкладок.
```
Запускаем chrome, всё должно работать!
Примечание: С какими проблемами вы столкнётесь?
* Первая жутко меня бесит, VirtualBox при установке и смене ядра выполняет свои скрипты в директории /tmp, приходится перемонтировать.
* Вторая: Обновление Nvidia, то же самое, но для компиляции драйвера можно переназначить директорию.
* Третья, клиент rocket chat на рабочий стол(так вот где собака зарыта!), не работает вообще. Пришлось его в LXC контейнер засунуть.
* Была проблема со steam но вроде пофиксили!

**Примечание: ПРИМЕЧАНИЕ: Технологии развиваются, разработка ведется непрерывно, универсального лекарства не существует, будьте внимательны!**
